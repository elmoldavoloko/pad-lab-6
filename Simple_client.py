import requests
from lxml import etree
from StringIO import *
from xml.dom.minidom import parseString

proxy_add = 'http://127.0.0.8'
xml_schema_add = 'Schema.xsd'

ronnie = '''    {
		"Department": "HR",
		"Name": "Ronnie",
		"Salary": 7500,
		"Surname": "O'Sullivan"
	}'''

marko = '''    {
		"Department": "Production",
		"Name": "Marko",
		"Salary": 3500,
		"Surname": "Fu"
	}'''

john = '''    {
		"Department": "Production",
		"Name": "John",
		"Salary": 5500,
		"Surname": "Higgins"
	}'''

asis = '''    {
		"Department": "P",
		"Name": "J",
		"Salary": 50,
		"Surname": "H",
		"id": 100
	}'''

xml = '''<?xml version="1.0" encoding="UTF-8" ?>
<root>
<item>
<Department>Aperture</Department>
<Salary>3000</Salary>
<Surname>Smith</Surname>
<Name>John</Name>
</item>
</root>'''

def validate(xml_doc, xml_schema):
	f = StringIO(xml_schema)
	xmlschema_doc = etree.parse(f)
	xmlschema = etree.XMLSchema(xmlschema_doc)

	valid = StringIO(xml_doc)
	doc = etree.parse(valid)
	return xmlschema.validate(doc)

def get(add):
	print 'GET ', add
	r = requests.get(add)
	dom = parseString(r.text)
	xml_doc = dom.toprettyxml()

	print xml_doc
	if validate(xml_doc, xml_schema):
		print "XML is valid"
	else:
		print "XML is invalid"

def get_all():
	add = proxy_add + '/worker/'
	get(add)

def get_one(id):
	add = proxy_add + '/worker/' + str(id) + '/'
	get(add)

def put_one(da):
	add = proxy_add + '/worker/'
	print 'PUT ', add
	print da
	r = requests.put( add, data = da )
	print 'id = ', r.text

def head_one(id):
	add = proxy_add + '/worker/' + str(id) + '/'
	print 'HEAD ', add
	r = requests.head( add )
	print r
	print r.headers


def test_xml(add):
	add = 'http://' + add

	print "Good"
	r = requests.get(add + '/worker/')
	print r
	print r.text

	r = requests.get(add + '/worker/10001/')
	print r
	print r.text

	r = requests.put(add + '/worker/', data = xml)
	print r
	print r.text

	# r = requests.put(add + '/worker/asis/', data = asis)
	# print r
	# print r.text	

	r = requests.head(add + '/worker/10001/')
	print r
	print r.headers


	print "Bad"
	r = requests.get(add + '/worker/1/')
	print r
	print r.text

	r = requests.head(add + '/worker/10/')
	print r
	print r.headers


	print "Wrong"
	r = requests.get(add + '/worer/')
	print r
	print r.text
	r = requests.get(add + '/worker')
	print r
	print r.text

	r = requests.get(add + '/worker/10001')
	print r
	print r.text
	r = requests.get(add + '/worer/10001/')
	print r
	print r.text

	r = requests.put(add + '/worer/', data = xml)
	print r
	print r.text
	r = requests.put(add + '/worker', data = xml)
	print r
	print r.text

	r = requests.head(add + '/worker/10001')
	print r
	print r.headers
	r = requests.head(add + '/worer/10001/')
	print r
	print r.headers

def test_json(add):
	add = 'http://' + add

	print "Good"
	r = requests.get(add + '/worker/')
	print r
	print r.text

	r = requests.get(add + '/worker/10001/')
	print r
	print r.text

	r = requests.put(add + '/worker/', data = john)
	print r
	print r.text

	# r = requests.put(add + '/worker/asis/', data = asis)
	# print r
	# print r.text	

	r = requests.head(add + '/worker/10001/')
	print r
	print r.headers


	print "Bad"
	r = requests.get(add + '/worker/1/')
	print r
	print r.text

	r = requests.head(add + '/worker/10/')
	print r
	print r.headers


	print "Wrong"
	r = requests.get(add + '/worer/')
	print r
	print r.text
	r = requests.get(add + '/worker')
	print r
	print r.text

	r = requests.get(add + '/worker/10001')
	print r
	print r.text
	r = requests.get(add + '/worer/10001/')
	print r
	print r.text

	r = requests.put(add + '/worer/', data = john)
	print r
	print r.text
	r = requests.put(add + '/worker', data = john)
	print r
	print r.text

	r = requests.head(add + '/worker/10001')
	print r
	print r.headers
	r = requests.head(add + '/worer/10001/')
	print r
	print r.headers


if __name__ == '__main__':

	with open(xml_schema_add, 'r') as f:
		xml_schema = f.read()

# get_all()

# get_one('10004')

# put_one(xml)

# head_one('10002')
